<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersRecords;
use DB;
class CardFormController extends Controller
{	

	public function __construct(Request $request)
    {
    	$this->records=New UsersRecords();
    }

	private $gameScore = [
		"player_score" => 0,
		"system_score" => 0
	];

	private $cards = ['2','3','4','5','6','7','8','9','10','J','Q','K','A'];

	public function index() {
		$user_records = DB::table('users_records')
                 ->select('*', DB::raw("SUM(users_records.win_status) as total,
                 	DATE_FORMAT(users_records.created_at, '%d-%b-%Y') as created_at") )
                 ->groupBy('name')
                 ->get();
                 
                 
        
		return response()->json($user_records, 200);	
	}	

    public function store(Request $request) {
        

        $this->validate($request, [
            'name' => 'required|string',
            'user_cards' => 'required | min:4',
            'system_cards' => 'required',
        ]);


        $player_hands = explode(' ', $request['user_cards']);
        $system_hands = explode(' ', $request['system_cards']);
        $win_status=0;
        

        $this->GetScore($player_hands,$system_hands);

        if($this->gameScore['player_score']>$this->gameScore['system_score'])
        	$win_status=1;	

        $player_record = $this->records;

        $player_record->name = $request->name;
        $player_record->user_score = $this->gameScore['player_score'];
        $player_record->system_score = $this->gameScore['system_score'];
        $player_record->win_status = $win_status;
        $player_record->save();


        return response()->json($this->gameScore, 200);
    }

    public function GetScore($player_hands,$system_hands) {

    	$player_cards_count = count($player_hands);
    	
		// Logic to identify which hand has higher value    	
        for($i=0 ; $i<count($player_hands) ; $i++){

    		if( array_search(strtoupper($player_hands[$i]), $this->cards) > 
    			array_search($system_hands[$i], $this->cards)) {
    			$this->gameScore['player_score']++;
    		}else if(array_search(strtoupper($player_hands[$i]), $this->cards) < 
    			array_search($system_hands[$i], $this->cards)) {
    			$this->gameScore['system_score']++;
    		}
    		else{
    			// Both the hands are same
    		}

        }

        return 1;
    }

}

